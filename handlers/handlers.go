package handlers

import (
	"net/http"

	"github.com/gorilla/mux"

	"gitbucket.com/smartmanager/controllers"
)

func Servers(w http.ResponseWriter, r *http.Request) {
	controllers.GetServersList(w, r)
}

func ServerDetail(w http.ResponseWriter, r *http.Request) {
	arg := mux.Vars(r)
	id := arg["id"]
	controllers.GetServerDetails(w, id)
}

func AddServer(w http.ResponseWriter, r *http.Request) {
	// arg := mux.Vars(r)
	// serverName := arg["servername"]

}
