package tests

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"gitbucket.com/smartmanager/handlers"
	"github.com/gorilla/mux"
)

var (
	database   = "smartmanager"
	dbpassword = "123123"
	dbuser     = "postgres"
	dbport     = "5432"
	dbhost     = "localhost"
)

func TestHandlerServer(t *testing.T) {
	mux := mux.NewRouter()
	mux.HandleFunc("/servers", handlers.Servers)
	ts := httptest.NewServer(mux)
	defer ts.Close()

	res, err := http.Get(ts.URL + "/servers")
	if err != nil {
		t.Log(err)
	}
	defer res.Body.Close()
	t.Log(res.Body)
	// if err != nil {
	// 	t.Error(err.Error())
	// }
	// fmt.Println(res)
	// servs, err := ioutil.ReadAll(res.Body)
	// if err != nil {
	// 	t.Fatal(err.Error())
	// }

}
