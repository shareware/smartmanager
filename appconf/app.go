package appconf

import (
	"fmt"
	"net/http"
	"net/http/httptest"

	"gitbucket.com/smartmanager/db"

	"github.com/gorilla/mux"
)

var MainApp *App

type App struct {
	server *httptest.Server
	router *mux.Router
	dBconf *db.Config
}

func New() *App {
	MainApp = &App{}
	return MainApp
}

func (app *App) InitializeRoter(mux *mux.Router) {
	MainApp.router = mux
}

func (app *App) InitializeDb(port, host, user, password, database string) {
	MainApp.dBconf = &db.Config{Port: port, Host: host, User: user, Password: password, Database: database}

}

func (app *App) GetDbConf() *db.Config {
	return MainApp.dBconf
}

func (app *App) SetTestServer(server *httptest.Server) {
	MainApp.server = server
}

func (app *App) Run(port string) {
	fmt.Println("POST=" + port)
	http.ListenAndServe(":"+port, app.router)
}
