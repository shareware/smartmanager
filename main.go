package main

import (
	"fmt"
	"log"
	"os"

	"gitbucket.com/smartmanager/appconf"
	"gitbucket.com/smartmanager/handlers"
	"github.com/gorilla/mux"
)

func main() {
	mux := mux.NewRouter()
	mux.HandleFunc("/servers", handlers.Servers)
	mux.HandleFunc("/servers/{id}", handlers.ServerDetail)
	appport := os.Getenv("APPPORT")

	dbport := os.Getenv("DBPORT")
	dbuser := os.Getenv("DBUSER")
	dbpassword := os.Getenv("DBPASSWORD")
	dbname := os.Getenv("DBNAME")
	dbhost := os.Getenv("DBHOST")
	if appport == "" || dbhost == "" || dbname == "" || dbpassword == "" || dbport == "" || dbuser == "" {
		log.Fatal("configuration not set")
	}
	fmt.Println(os.Environ())
	App := appconf.New()
	App.InitializeRoter(mux)
	App.InitializeDb(dbport, dbhost, dbuser, dbpassword, dbname)
	App.Run(appport)
}
