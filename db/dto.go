package db

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

var (
	defaultCfg = Config{
		Password: "123123",
		Database: "smartmanager",
		User:     "postgres",
		Port:     "5432",
		Host:     "localhost",
	}

	selectServers = "select id,name,detailsid from servers"
	selectDetails = "select * from details where id="
)

//Dto содержит соединение c базой созданное на основе Config
type Dto struct {
	Db  *sql.DB
	Cfg Config
}

//Config содержит конфигурацию соединения с базой
type Config struct {
	Port     string
	Host     string
	User     string
	Password string
	Database string
}

//NewConnection Создает новое подключение к указаной базе данных
func NewDefaultConnection() (*Dto, error) {
	cfg := defaultCfg
	dbinfo := fmt.Sprintf("user=%s password=%s port=%s host=%s  dbname=%s  sslmode=disable", cfg.User, cfg.Password, cfg.Port, cfg.Host, cfg.Database)
	var dbDto Dto
	var err error
	dbDto.Db, err = sql.Open("postgres", dbinfo)
	if err != nil {
		log.Fatal(err.Error())
	}
	err = dbDto.Db.Ping()
	if err != nil {
		log.Fatal(err.Error())
	}

	return &dbDto, nil
}

//NewConnection Создает новое подключение к указаной базе данных
func NewConnection(cfg *Config) (*Dto, error) {
	if cfg.Database == "" || cfg.Host == "" || cfg.Password == "" || cfg.Port == "" || cfg.User == "" {
		log.Fatal("cfg not set")
	}
	dbinfo := fmt.Sprintf("user=%s password=%s port=%s host=%s dbname=%s sslmode=disable", cfg.User, cfg.Password, cfg.Port, cfg.Host, cfg.Database)
	var dbDto Dto
	var err error
	dbDto.Db, err = sql.Open("postgres", dbinfo)
	if err != nil {
		log.Fatal(err.Error())
	}
	// err = dbDto.Db.Ping()
	// if err != nil {
	// 	log.Fatal(err.Error())
	// }

	return &dbDto, nil
}

//Close Закрывает подлючение к базе данных
func (dbDto *Dto) Close() {
	dbDto.Db.Close()
}

//GetDetails Получает информацию о сервере
func (dbDto *Dto) GetDetails(id string) (Details, error) {
	rows, err := dbDto.Db.Query(selectDetails + id)

	if err != nil {
		fmt.Println(err.Error())
	}

	var details Details
	for rows.Next() {
		err = rows.Scan(&details.ID, &details.ContractNum, &details.WarrantyTerm, &details.ActCreated,
			&details.MaintenanceNum, &details.ActTerm, &details.ActWith, &details.ActSignDate,
			&details.СhannelWidth, &details.TopologyParent, &details.PDCversion)
		// err = rows.Scan(details)
		if err != nil {
			fmt.Println(err.Error())
		}
	}
	return details, nil
}

//GetServers Получает список серверов
func (dbDto *Dto) GetServers() ([]Server, error) {
	rows, err := dbDto.Db.Query(selectServers)
	if err != nil {
		return nil, err
	}
	var servers []Server
	for rows.Next() {
		var server Server
		err = rows.Scan(&server.ID, &server.Name, &server.DetailsID)
		servers = append(servers, server)
	}
	fmt.Println(servers)
	return servers, err
}
