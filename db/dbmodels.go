package db

import "time"

type Server struct {
	ID        int
	Name      string
	DetailsID int
}

//Details
type Details struct {
	ID             int
	ContractNum    string
	WarrantyTerm   int
	ActCreated     time.Time
	MaintenanceNum string
	ActTerm        int
	ActWith        string
	ActSignDate    time.Time
	СhannelWidth   int
	TopologyParent string
	PDCversion     string
}
