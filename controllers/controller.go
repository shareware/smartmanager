package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitbucket.com/smartmanager/appconf"

	"gitbucket.com/smartmanager/db"
)

func GetServerDetails(w http.ResponseWriter, id string) {
	dbdto, err := db.NewDefaultConnection()
	checkErr(err)
	details, err := dbdto.GetDetails(id)
	checkErr(err)
	b, err := json.Marshal(details)
	checkErr(err)
	_, err = w.Write(b)
	checkErr(err)
}

func GetServersList(w http.ResponseWriter, r *http.Request) {
	fmt.Println(appconf.MainApp.GetDbConf())
	dbdto, err := db.NewConnection(appconf.MainApp.GetDbConf())
	checkErr(err)
	servers, err := dbdto.GetServers()
	checkErr(err)
	b, err := json.Marshal(servers)
	checkErr(err)
	fmt.Println(b)
	_, err = w.Write(b)
	checkErr(err)

}

func checkErr(err error) {
	if err != nil {
		fmt.Println(err.Error())
	}
}
